# Enceladus Depths

Play in browser: https://trydev.itch.io/enceladus-depths

Made in 2 days for Ludum Dare 48 Compo: https://ldjam.com/events/ludum-dare/48/enceladus-depths

![Gameplay gif](https://img.itch.zone/aW1hZ2UvMTAwNzg1MC81NzcxMzgzLmdpZg==/347x500/JKBLcz.gif)

![Gameplay gif](https://img.itch.zone/aW1hZ2UvMTAwNzg1MC81NzcxNDE4LmdpZg==/347x500/M23Vr0.gif)

# License
- [MIT](LICENSE)
