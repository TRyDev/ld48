extends CanvasLayer

onready var score_label = $Screen/VBoxContainer/HBoxContainer/Score
onready var deaths_label = $Screen/VBoxContainer/HBoxContainer/Deaths
onready var wave_progress = $Screen/VBoxContainer/WaveProgress


func _ready():
	_on_score_updated(0)
	_on_player_deaths_updated(0)

func _on_score_updated(score):
	score_label.text = str(score)

func _on_player_deaths_updated(count):
	deaths_label.text = str(count)

func _on_wave_progress_updated(current, total) -> void:
	var perc = float(current) / total
	wave_progress.value = perc
#	wave_label.text = "%s / %s (%s)" %[current, total, perc]
