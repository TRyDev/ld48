extends CanvasLayer

onready var rect = $Screen/ColorRect
onready var glitch_timer = $GlitchTimer

const MAX_IDX = 5
const MIN_VIGNETTE = 0.1
const MAX_VIGNETTE = 0.5

const START_GLITCH_FREQ = 1 #10
const END_GLITCH_FREQ = 1# 5
const GLITCH_FREQ_MULT_MIN = 0.8
const GLITCH_FREQ_MULT_MAX = 1.2
const GLITCH_MIN_DURATION = 0.2
const GLITCH_MAX_DURATION = 0.4

const GLITCH_START_AMPLITUDE = 0.5
const GLITCH_END_AMPLITUDE = 1

var t = 0.0

func _ready():
	_on_wave_started(0)

func _on_wave_started(idx):
	t = float(idx) / MAX_IDX
	var v_opacity = Math.map(t, 0.0, 1.0, MIN_VIGNETTE, MAX_VIGNETTE)
	rect.material.set_shader_param("vignette_opacity", v_opacity)
#	randomize_glitch_wait_time()
#
#func _on_GlitchTimer_timeout():
#	var duration = Rng.randf(GLITCH_MIN_DURATION, GLITCH_MAX_DURATION)
#	var ampl = Math.map(t, 0.0, 1.0, GLITCH_START_AMPLITUDE, GLITCH_END_AMPLITUDE)
#	rect.material.set_shader_param("aberration_amount", ampl)
#	print("Glitch start. Amp %s. Duration %s" %[ampl, duration])
#	yield(get_tree().create_timer(duration), "timeout")
#	rect.material.set_shader_param("aberration_amount", 0)
#	randomize_glitch_wait_time()
#
#func randomize_glitch_wait_time():
#	var wait_time = Math.map(t, 0.0, 1.0, START_GLITCH_FREQ, END_GLITCH_FREQ)
#	wait_time *= Rng.randf(GLITCH_FREQ_MULT_MIN, GLITCH_FREQ_MULT_MAX)
#	glitch_timer.stop()
#	glitch_timer.wait_time = wait_time
#	glitch_timer.start()
