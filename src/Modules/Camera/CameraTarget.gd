extends Boid

# Spring-like offset for camera
# Set as target for CameraController

export (int, 0, 100) var max_offset_length := 20

onready var base_transform = get_parent()
var _offset := Vector2()

const MIN_MOVE_DIST_SQ := 2.0

func set_offset(dir: Vector2, length: float) -> void:
	length = clamp(length, 0.0, max_offset_length)
	dir = dir.normalized()
	_offset = dir * length

func update() -> void:
	var target_pos = base_transform.global_position + _offset
	# Apply force only if far enough from target
	if (global_position - target_pos).length_squared() > MIN_MOVE_DIST_SQ:
		apply_force(arrive(target_pos))
	.update()

func force_reset_offset() -> void:
	set_offset(Vector2.ZERO, 0.0)
	while position.length_squared() > Math.EPSILON:
		update()
		yield(get_tree(), "idle_frame")
	position = Vector2.ZERO
