extends Node

# Speedrun Module that can be easily hacked by changing system time
# Uses msec as unit of time

var _start_time := -1
var _stop_time := -1

var _paused_time := -1
var _paused_duration := 0

func _init():
	reset()

func reset() -> void:
	_start_time = -1
	_stop_time = -1
	_paused_time = -1
	_paused_duration = 0

func start() -> void:
	_start_time = _current_time()

func stop() -> void:
	_stop_time = _current_time()

func get_time() -> int:
	if _stop_time < 0:
		return _current_time() - _start_time - _paused_duration
	return _stop_time - _start_time - _paused_duration

func get_time_string() -> String:
	var t = get_time()
	var msecs = t % 1000
	var secs = t / 1000
	return str("%s:%s" % [secs, msecs])

func pause() -> void:
	if _paused_time >= 0:
		print("Speedrun timer is already paused!")
		return
	_paused_time = _current_time()

func unpause() -> void:
	if _paused_time < 0:
		print("Speedrun timer is not paused!")
		return
	_paused_duration += _current_time() - _paused_time
	_paused_time = -1

func _current_time() -> int:
	return OS.get_system_time_msecs()
