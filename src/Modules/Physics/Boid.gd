extends PhysicalObject2D

# Source: https://natureofcode.com/book/chapter-6-autonomous-agents/

class_name Boid

export(float, 0.0, 100.0) var slow_radius := 30.0

func seek(target: Vector2) -> Vector2:
	var desired = target - global_position
	desired = desired.normalized()
	desired *= max_speed
	var steer = desired - velocity
	return steer

func arrive(target: Vector2) -> Vector2:
	var desired = target - global_position
	var d = desired.length()
	desired = desired.normalized()
	if d < slow_radius:
		var m = Math.map(d, 0, slow_radius, 0, max_speed)
		desired *= m
	else:
		desired *= max_speed
	var steer = desired - velocity
	return steer

func flow(flow: FlowField) -> Vector2:
	var desired = flow.lookup(global_position)
	if desired.length_squared() < Math.EPSILON:
		return Vector2()
	desired *= max_speed
	var steer = desired - velocity
	return steer
