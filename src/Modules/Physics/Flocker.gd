extends Boid

class_name Flocker

export(float, 0.0, 100.0) var separate_radius := 20.0

export(float, 0.0, 10.0) var sep_mult := 1.0
export(float, 0.0, 10.0) var ali_mult := 1.0
export(float, 0.0, 10.0) var coh_mult := 1.0

var neighbors = []

func flock() -> void:
	var sep = separate()
	var ali = align()
	var coh = cohesion()
	apply_force(sep * sep_mult)
	apply_force(ali * ali_mult)
	apply_force(coh * coh_mult)

func separate() -> Vector2:
	if not neighbors:
		return Vector2()
	var sum = Vector2()
	var count = 0
	for b in neighbors:
		var diff = global_position - b.global_position
		if diff.length_squared() < pow(separate_radius, 2.0):
			sum += diff.normalized()
			count += 1
	if not count:
		return Vector2()
	sum /= len(neighbors)
	sum *= max_speed
	var steer = sum - velocity
	return steer

func align() -> Vector2:
	if not neighbors:
		return Vector2()
	var sum = Vector2()
	for n in neighbors:
		sum += n.velocity
	sum /= len(neighbors)
	sum = sum.normalized() * max_speed
	var steer = sum - velocity
	return steer

func cohesion() -> Vector2:
	if not neighbors:
		return Vector2()
	var sum = Vector2()
	for n in neighbors:
		sum += n.position
	sum /= len(neighbors)
	return arrive(sum)

func _on_SeeArea_entered(area):
	neighbors.append(area.get_parent())

func _on_SeeArea_exited(area):
	neighbors.erase(area.get_parent())
