extends Node

onready var _audio_player :AudioStreamPlayer = $AudioStreamPlayer
onready var _volume_tween :Tween = $VolumeTween

var _songs := {}

const SONGS_PATH := "res://assets/audio/music"
const DEFAULT_VOLUME_LERP_DURATION := 12.0
export(int, -80, 0) var BASE_VOLUME_DB := -20

func _ready():
	self.load()
	lerp_volume_default(0.0)

func play(name :String) -> void:
	if name in _songs:
		# If currently playing "name", do nothing (return)
		if _audio_player.stream and not _audio_player.stream_paused:
			var current_name = FileSystem.file_name_from_path(_audio_player.stream.resource_path)
			if current_name == name:
				return
		pause(false)
		_audio_player.stream = _songs[name]
		_audio_player.play()
	else:
		push_error("HEY! No such song: %s" % name)

func pause(val: bool) -> void:
	if _audio_player.stream_paused or _audio_player.playing:
		_audio_player.stream_paused = val

func stop() -> void:
	_audio_player.stop()
	_audio_player.stream = null
	_audio_player.stream_paused = false

func lerp_volume(volume := BASE_VOLUME_DB, duration := DEFAULT_VOLUME_LERP_DURATION) -> void:
	# https://github.com/godotengine/godot/issues/32882
	# https://www.reddit.com/r/godot/comments/fpmwjx/tweening_audio_bus_volume/
	# Hardcoded solution for Music bus
	volume = clamp(volume, -80.0, 0.0)
	_volume_tween.stop_all()
	if duration <= 0.0:
		_audio_player.volume_db = volume
	else:
		var current_volume_db = _audio_player.volume_db
		_volume_tween.interpolate_property(_audio_player, "volume_db", current_volume_db, volume, duration, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		_volume_tween.start()

func lerp_volume_default(duration := DEFAULT_VOLUME_LERP_DURATION) -> void:
	lerp_volume(BASE_VOLUME_DB, duration)

func load() -> void:
	for f in FileSystem.get_files(SONGS_PATH):
		if f.ends_with(".ogg"):
			var song_name = f.split(".")[0]
			var song_path = FileSystem.concat_path([SONGS_PATH, f])
			_songs[song_name] = load(song_path)
