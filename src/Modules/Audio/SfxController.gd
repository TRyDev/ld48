extends Node2D

onready var _sources_0d = $Sources0D
onready var _sources_2d = $Sources2D

var _clips := {}

const MAX_AUDIO_SOURCES := 8
const BASE_VOLUME_DB := -30
const MIN_PITCH := 0.5
const MAX_PITCH := 1.5
const SFX_PATH := "res://assets/audio/sfx"
const SFX_BUS := "Sfx"

const SOURCE_2D_MAX_DISTANCE := 500.0
const SOURCE_2D_ATTENUATION := 1.0

func _init():
	self.load()

func play(name: String, rand_pitch := false) -> void:
	if not name:
		return
	if not name in _clips:
		push_error("HEY! No such audio clip: %s" %name)
		return
	var source = _get_idle_source(_sources_0d, AudioStreamPlayer)
	_play_audio(source, name, rand_pitch)

func play_at(name: String, position: Vector2, rand_pitch := false) -> void:
	if not name in _clips:
		push_error("HEY! No such audio clip: %s" %name)
		return
	var source = _get_idle_source(_sources_2d, AudioStreamPlayer2D)
	if source:
		source.global_position = position
		_play_audio(source, name, rand_pitch)

func _get_idle_source(sources_parent, template):
	var sources = sources_parent.get_children()
	for source in sources:
		if not source.playing:
			return source
	if len(sources) < MAX_AUDIO_SOURCES:
		var new_source = template.new()
		new_source.bus = SFX_BUS
		new_source.volume_db = BASE_VOLUME_DB
		sources_parent.add_child(new_source)
		if template == AudioStreamPlayer2D:
			new_source.max_distance = SOURCE_2D_MAX_DISTANCE
			new_source.attenuation = SOURCE_2D_ATTENUATION
		return new_source
	else:
		push_error("HEY! Trying to play more than %s audio clips at the same time" % MAX_AUDIO_SOURCES)

func _play_audio(source, name: String, rand_pitch: bool):
	if not source:
		return
	source.stream = Rng.rand_from_array(_clips[name])
	if rand_pitch:
		source.pitch_scale = Rng.randf(MIN_PITCH, MAX_PITCH)
	else:
		source.pitch_scale = 1.0
	source.play()

func load():
	# Load directories
	for dir in FileSystem.get_directories(SFX_PATH):
		_clips[dir] = []
		for f in FileSystem.get_files(FileSystem.concat_path([SFX_PATH, dir])):
			if f.ends_with(".wav"):
				var clip = load(FileSystem.concat_path([SFX_PATH, dir, f]))
				_clips[dir].append(clip)
		if not _clips[dir]:
			_clips.erase(dir)
	# Load single clips
	for clip_file in FileSystem.get_files(SFX_PATH):
		if clip_file.ends_with(".wav"):
			var clip_name = clip_file.split('.')[0]
			var clip_path = FileSystem.concat_path([SFX_PATH, clip_file])
			var clip = load(clip_path)
			if _clips.has(clip_name):
				print("HEY! Audio clip %s is both directory and single clip. Merging directory with single clip.")
				_clips[clip_name].append(clip)
			else:
				_clips[clip_name] = [clip]
