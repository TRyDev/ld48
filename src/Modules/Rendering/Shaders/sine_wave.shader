shader_type canvas_item;

// From: https://github.com/GDQuest/godot-demos/blob/master/2018/06-16-intro-to-shaders-2d-water/end/water/01.sine/water_sine_function.shader

uniform vec2 time_factor = vec2(2.0, 3.0);
uniform vec2 offset_factor = vec2(5.0, 2.0);
uniform vec2 amplitude = vec2(0.05, 0.05);

vec2 calculate_wave_uv_offset(in float time, vec2 source_uvs, vec2 time_multiplier, vec2 waves_scale) {
	return vec2(
		sin(time * time_multiplier.x + (source_uvs.x + source_uvs.y) * waves_scale.x),
		cos(time * time_multiplier.y + (source_uvs.x + source_uvs.y) * waves_scale.y)
	);
}

void fragment() {
	vec2 wave_uv_offset = calculate_wave_uv_offset(TIME, SCREEN_UV, time_factor, offset_factor);
	COLOR = texture(SCREEN_TEXTURE, SCREEN_UV + wave_uv_offset * amplitude);
}