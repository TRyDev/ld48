shader_type canvas_item;
// From: https://gist.github.com/jcdickinson/580b7fb5cc145cee8740

uniform bool apply_protanope = false;
uniform bool apply_deuteranope = false;
uniform bool apply_tritanope = false;

uniform bool apply_correction = false;


vec3 rgb_to_lms(vec3 col) {
	return vec3(
		(17.8824f * col.r) + (43.5161f * col.g) + (4.11935f * col.b),
		(3.45565f * col.r) + (27.1554f * col.g) + (3.86714f * col.b),
		(0.0299566f * col.r) + (0.184309f * col.g) + (1.46709f * col.b)
	);
}

// l - long, m - medium, s - short wavelengths
vec3 lms_to_rgb(vec3 col) {
	return vec3(
		(0.0809444479f * col.r) + (-0.130504409f * col.g) + (0.116721066f * col.b),
		(-0.0102485335f * col.r) + (0.0540193266f * col.g) + (-0.113614708f * col.b),
		(-0.000365296938f * col.r) + (-0.00412161469f * col.g) + (0.693511405f * col.b)
	);
}

// Protanope - reds are greatly reduced (1% men)
vec3 lms_to_protanope(vec3 col) { 
	return vec3(
		0.0f * col.r + 2.02344f * col.g + -2.52581f * col.b,
		0.0f * col.r + 1.0f * col.g + 0.0f * col.b,
		0.0f * col.r + 0.0f * col.g + 1.0f * col.b
	);
}

// Deuteranope - greens are greatly reduced (1% men)
vec3 lms_to_deuteranope(vec3 col) {
	return vec3(
		1.0f * col.r + 0.0f * col.g + 0.0f * col.b,
		0.494207f * col.r + 0.0f * col.g + 1.24827f * col.b,
		0.0f * col.r + 0.0f * col.g + 1.0f * col.b
	);
}

// Tritanope - blues are greatly reduced (0.003% population)
vec3 lms_to_tritanope(vec3 col) {
	return vec3(
		1.0f * col.r + 0.0f * col.g + 0.0f * col.b,
		0.0f * col.r + 1.0f * col.g + 0.0f * col.b,
		-0.395913f * col.r + 0.801109f * col.g + 0.0f * col.b
	);
}

vec3 correction(vec3 input, vec3 col) {
	vec3 error = input - col;
	vec3 correction = vec3(
		0.0,
		(error.r * 0.7) + (error.g * 1.0),
		(error.r * 0.7) + (error.b * 1.0)
	);
	return input + correction;
}

void fragment() {
	vec3 input = texture(SCREEN_TEXTURE, SCREEN_UV).rgb;
	vec3 col = input;
	col = rgb_to_lms(col);
	if (apply_protanope) {
		col = lms_to_protanope(col);
	}
	if (apply_deuteranope) {
		col = lms_to_deuteranope(col);
	}
	if (apply_tritanope) {
		col = lms_to_tritanope(col);
	}
	if (apply_correction) {
		col = correction(input, col);
	} else {
		col = lms_to_rgb(col);
	}
	COLOR = vec4(col, 1.0);
}
