shader_type canvas_item;

uniform vec2 screen_size = vec2(1366.0, 768.0);
uniform float pixel_size = 8.0;

void fragment() {
	float dx = pixel_size * (1.0 / screen_size.x);
	float dy = pixel_size * (1.0 / screen_size.y);
	vec2 coord = vec2(dx * floor(UV.x / dx),
					  1.0 - dy * floor(UV.y / dy));
	vec3 col = texture(SCREEN_TEXTURE, coord).rgb;
	COLOR = vec4(col, 1.0);
}