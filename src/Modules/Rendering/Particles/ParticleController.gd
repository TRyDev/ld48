extends Node

var _particles = {}
const PARTICLES_DIR = "res://src/Modules/Rendering/Particles"

func _ready():
	self.load()

func play(name: String, position: Vector2) -> void:
	if not name in _particles:
		push_error("HEY! Trying to play nonexistant particle: %s" %name) 
		return
	_create_particle(name, position)

func _create_particle(name: String, position: Vector2) -> void:
	var new_particle = _particles[name].instance()
	add_child(new_particle)
	new_particle.global_position = position
	new_particle.emitting = true
	if not new_particle.one_shot:
		print("HEY! Particle %s is not set to `one_shot`, but will be destroyed after one cycle anyway!" %name)
	yield(get_tree().create_timer(new_particle.lifetime), "timeout")
	new_particle.queue_free()

func load():
	for f in FileSystem.get_files(PARTICLES_DIR):
		if f.ends_with(".tscn"):
			var particle_name = f.split(".")[0].to_lower()
			var particle_path = FileSystem.concat_path([PARTICLES_DIR, f])
			var particle = load(particle_path)
			if _particles.has(particle_name):
				print("HEY! particle %s already exists" %particle_name)
				continue
			_particles[particle_name] = particle
