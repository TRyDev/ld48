extends CanvasLayer

# Full screen pixelize. Set layer so that UI is rendered in front of this effect
# Resizing won't work (and is not needed) for 2d games that have set:
# display/window/stretch/mode = viewport
# display/window/stretch/aspect = keep


onready var px = $Screen/Pixelize

func _ready() -> void:
	get_tree().root.connect("size_changed", self, "update_screen_size")
	update_screen_size()

func update_screen_size() -> void:
	var screen_size = get_viewport().size
	px.material.set_shader_param("screen_size", screen_size)

func get_pixel_size() -> int:
	return px.material.get_shader_param("pixel_size")

func set_pixel_size(val: int) ->void:
	px.material.set_shader_param("pixel_size", val)
