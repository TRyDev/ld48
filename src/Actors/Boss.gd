extends Enemy

onready var weapon_controller_2 = $WeaponController2
onready var weapon_controller_center = $WeaponController3

func _ready():
	weapon_controller_2.connect("bullet_created", self, "_on_bullet_created")
	weapon_controller_center.connect("bullet_created", self, "_on_bullet_created")
	SfxController.play("boss")
	._ready()

func shoot():
	if Rng.randf() < CENTER_WEAPON_SHOOT_CHANCE:
		weapon_controller_center.shoot()
	else:
		weapon_controller.shoot()
		weapon_controller_2.shoot()
