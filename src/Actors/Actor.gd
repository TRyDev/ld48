extends PhysicalObject2D

signal actor_destroyed(actor)

export(Vector2) var move_force := Vector2(15.0, 10.0)

onready var weapon_controller = $WeaponController
onready var anim_player = $AnimationPlayer
onready var sprite = $Sprite
onready var coll_shape = $Collisions/CollisionShape2D

class_name Actor

func shoot():
	weapon_controller.shoot()

func _on_Collisions_area_entered(area):
#	print("%s collided with %s" %[name, area.name])
	destroy()

func destroy():
	coll_shape.set_deferred("disabled", true)
	AnimationController.play(anim_player, "destroy", false)
	emit_signal("actor_destroyed", self)
	yield(anim_player, "animation_finished")
	queue_free()
