extends Area2D

signal bullet_created(b)

export(PackedScene) var bullet
export(float, 0.1, 2.0) var fire_rate := 0.5
export(Vector2) var bullet_dir := Vector2.ZERO
export(String) var shoot_sfx := ""

onready var fire_rate_timer = $FireRateTimer

func _ready():
	fire_rate_timer.wait_time = fire_rate

func shoot():
	if not fire_rate_timer.is_stopped():
		return
	fire_rate_timer.start()
	spawn_bullet()

func spawn_bullet():
	if not bullet:
#		print("HEY! No bullet to shoot")
		return
	SfxController.play(shoot_sfx, true)
	var new_bullet = bullet.instance()
	get_tree().root.add_child(new_bullet)
	emit_signal("bullet_created", new_bullet)
	new_bullet.initialize(global_position, bullet_dir.normalized(), collision_layer, collision_mask)
