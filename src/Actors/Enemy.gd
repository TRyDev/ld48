extends Actor

class_name Enemy

signal health_updated(current, start)

onready var action_timer = $ActionCooldownTimer
enum EnemyActions {
	IDLE, SHOOT, EVADE, FALLBACK, MULTISHOT
}
export(int, FLAGS, "IDLE", "SHOOT", "EVADE", "FALLBACK", "MULTISHOT") var available_actions := 0
var actions_array = []

export(int, 1, 60) var start_health = 1
export(float, 0.0, 1000.0) var evade_force_mag := 200.0
export(int, 1, 60) var evade_impulses = 15
export(float, 0.0, 100.0) var force_mult := 10.0
export(int, 1, 10) var multishot_count = 5
export(float, 0.1, 0.5) var multishot_cooldown = 0.2
export(int, 1, 100) var kill_score = 1

const MAX_CENTER_X_DISTANCE := 45.0
const FALLBACK_START_POS_Y = 0.0
const FALLBACK_MULT = 3.0
const FORCE_FALLBACK_POS_Y = -15

const CENTER_WEAPON_SHOOT_CHANCE = 0.5

var active_bullets = []
var current_health = -1

# Called before ready
func initialize(pos: Vector2) -> void:
	global_position = pos
	for i in 10:
		if available_actions & (1 << i) != 0:
			actions_array.append(i)

func _ready():
	AnimationController.reset(anim_player)
	weapon_controller.connect("bullet_created", self, "_on_bullet_created")
	current_health = start_health

func _physics_process(delta):
#	shoot()
	apply_force(move_force * force_mult)
	update()

func _on_Collisions_area_entered(area):
	if area.get_parent() is Player:
		return
	current_health -= 1
	emit_signal("health_updated", current_health, start_health)
	if current_health <= 0:
		destroy()

func _on_ActionCooldownTimer_timeout():
	if len(actions_array) < 1:
		disable_actions()
		return
	var action_id = Rng.rand_from_array(actions_array)
	if global_position.y < FORCE_FALLBACK_POS_Y and actions_array.has(EnemyActions.FALLBACK):
		action_id = EnemyActions.FALLBACK
	match action_id:
		EnemyActions.IDLE:
			pass
		EnemyActions.SHOOT:
			shoot()
		EnemyActions.EVADE:
			var x_dir = 0
			if global_position.x >= MAX_CENTER_X_DISTANCE:
				x_dir = -1
			elif global_position.x <= -MAX_CENTER_X_DISTANCE:
				x_dir = 1
			else:
				x_dir = Rng.rand_from_array([-1, 1])
			_evade(Vector2(x_dir, 0.0), Rng.randf(0.8, 1.0))
		EnemyActions.FALLBACK:
			if global_position.y <= FALLBACK_START_POS_Y:
				_evade(Vector2(0.0, 1.0), FALLBACK_MULT)
		EnemyActions.MULTISHOT:
			for i in range(multishot_count):
				shoot()
				yield(get_tree().create_timer(multishot_cooldown), "timeout")

func _evade(dir, mult := 1.0):
	var force = dir.normalized() * evade_force_mag * mult / evade_impulses
	for i in range(evade_impulses):
		apply_force(force)
		yield(get_tree(), "idle_frame")

func disable_actions():
	action_timer.stop()

func destroy_active_bullets():
	for b in active_bullets:
		b.queue_free()

func destroy():
	action_timer.stop()
	SfxController.play("enemy_hurt", true)
	.destroy()

func shoot():
	weapon_controller.shoot()

func _on_bullet_created(b):
	b.connect("bullet_destroyed", self, "_on_bullet_destroyed", [b])
	active_bullets.append(b)

func _on_bullet_destroyed(b):
	active_bullets.erase(b)
