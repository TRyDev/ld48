extends Enemy

var rotation_speed = Rng.randf(-PI, PI)

const MAX_MOVE_FORCE_X = 0.3

func _ready():
	move_force.x = Rng.randf(-MAX_MOVE_FORCE_X, MAX_MOVE_FORCE_X)

func _process(delta):
	sprite.rotation += rotation_speed * delta
	rotation_speed = lerp(rotation_speed, 0.0, delta)
	if rotation_speed < 0.01 and rotation_speed > -0.01:
		set_process(false)
