extends Sprite

onready var anim_player = $AnimationPlayer

func _ready():
#	AnimationController.reset(anim_player)
#	yield(get_tree(), "idle_frame")
	AnimationController.play(anim_player, "play", false)
	yield(anim_player, "animation_finished")
	queue_free()
