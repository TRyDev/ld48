extends CanvasLayer

onready var bar = $Screen/ProgressBar
onready var anim_player = $AnimationPlayer

func _ready():
	get_parent().connect("health_updated", self, "_on_health_updated")
	# Init
	bar.max_value = get_parent().start_health
	bar.value = get_parent().start_health
	AnimationController.play(anim_player, "show")

func _on_health_updated(current, start):
	bar.value = current
