extends Actor

class_name Player

func _ready():
	AnimationController.reset(anim_player)

func _physics_process(delta):
	update()

func update():
	if Input.is_action_pressed("space"):
		shoot()
	var force = _get_input() * move_force
	apply_force(force)
	.update()

func _get_input() -> Vector2:
	return Vector2(
		Input.get_action_strength("right") - Input.get_action_strength("left"),
		Input.get_action_strength("down") - Input.get_action_strength("up")
	)

func destroy():
	SfxController.play("player_hurt")
	ParticleManager.play_explosion(global_position)
	.destroy()
#	emit_signal("actor_destroyed", self)
#	print("Should destroy player")

func set_invincible(val: bool):
	coll_shape.set_deferred("disabled", val)
