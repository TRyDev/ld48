extends PhysicalObject2D

signal bullet_destroyed()

class_name Bullet

onready var collision_area = $CollisionArea
onready var anim_player = $AnimationPlayer
onready var coll_shape = collision_area.get_node("CollisionShape2D")

export(float, 0.0, 1000.0) var start_speed = 200.0

func initialize(pos: Vector2, shoot_dir: Vector2, col_layer: int, col_mask: int):
	global_position = pos
	collision_area.collision_layer = col_layer
	collision_area.collision_mask = col_mask
	velocity = shoot_dir * start_speed
	AnimationController.reset(anim_player)

func _physics_process(delta):
	update()

func _on_CollisionArea_area_entered(area):
	var mid_pos = 0.5 * (global_position + area.global_position)
	ParticleManager.play_explosion(mid_pos)
	destroy()

func destroy():
	emit_signal("bullet_destroyed")
#	velocity = Vector2.ZERO
	velocity *= 0.5
	coll_shape.set_deferred("disabled", true)
	AnimationController.play(anim_player, "destroy", false)
	yield(anim_player, "animation_finished")
	queue_free()
