extends Node2D

signal wave_finished()
signal wave_progress_updated(current, total)

export(String) var music_track = ""

var world

var active_spawner_count = 0

var max_enemies_spawned = 0
var enemies_destroyed = 0

const WAVE_START_DELAY = 0.5

func initialize(_world):
	world = _world
	active_spawner_count = get_child_count()
	for spawner in get_children():
		spawner.initialize(_world)
		max_enemies_spawned += spawner.total_spawned
		spawner.connect("finished_spawning", self, "_on_spawner_finished_spawning")
		spawner.connect("enemy_destroyed", self, "_on_enemy_destroyed")

func start_wave():
	yield(get_tree().create_timer(WAVE_START_DELAY), "timeout")
	if active_spawner_count <= 0:
		emit_signal("wave_finished")
	else:
		for spawner in get_children():
			spawner.start_spawning()
	if music_track:
		MusicOrchestrator.set_track(music_track, true)
	enemies_destroyed = 0
	emit_signal("wave_progress_updated", 0, max_enemies_spawned)

func stop_wave():
	enemies_destroyed = 0
	active_spawner_count = get_child_count()
	for spawner in get_children():
		spawner.stop_spawning()
		spawner.reset()
	yield(get_tree().create_timer(1.5), "timeout")
	for spawner in get_children():
		spawner.destroy_all_entities()

func _on_spawner_finished_spawning():
	active_spawner_count -= 1
	if active_spawner_count <= 0:
		emit_signal("wave_finished")

func _on_enemy_destroyed():
	enemies_destroyed += 1
	emit_signal("wave_progress_updated", enemies_destroyed, max_enemies_spawned)
