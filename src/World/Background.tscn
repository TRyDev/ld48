[gd_scene load_steps=10 format=2]

[ext_resource path="res://assets/art/background1.png" type="Texture" id=1]
[ext_resource path="res://src/World/Background.gd" type="Script" id=2]
[ext_resource path="res://assets/art/background2.png" type="Texture" id=3]
[ext_resource path="res://src/World/ParallaxBg.gd" type="Script" id=4]
[ext_resource path="res://assets/art/background3.png" type="Texture" id=5]
[ext_resource path="res://src/UserInterface/Crt.tscn" type="PackedScene" id=6]

[sub_resource type="Shader" id=1]
code = "/*
Godot 3 2D CRT Shader.
A 2D shader for Godot 3 simulating a CRT..
Author: hiulit
Repository: https://github.com/hiulit/Godot-3-2D-CRT-Shader
Issues: https://github.com/hiulit/Godot-3-2D-CRT-Shader/issues
License: MIT https://github.com/hiulit/Godot-3-2D-CRT-Shader/blob/master/LICENSE
*/

shader_type canvas_item;

const float PI = 3.14159265359;

uniform vec2 screen_size = vec2(320.0, 180.0);
uniform bool show_curvature = true;
uniform float curvature_x_amount : hint_range(3.0, 15.0, 0.01) = float(6.0); 
uniform float curvature_y_amount : hint_range(3.0, 15.0, 0.01) = float(4.0);
uniform vec4 corner_color : hint_color = vec4(0.0, 0.0, 0.0, 1.0);
uniform bool show_vignette = true;
uniform float vignette_opacity : hint_range(0.0, 1.0, 0.01) = 0.2;
uniform bool show_horizontal_scan_lines = true;
uniform float horizontal_scan_lines_amount : hint_range(0.0, 180.0, 0.1) = 180.0;
uniform float horizontal_scan_lines_opacity : hint_range(0.0, 1.0, 0.01) = 1.0;
uniform bool show_vertical_scan_lines = false;
uniform float vertical_scan_lines_amount : hint_range(0.0, 320.0, 0.1) = 320.0;
uniform float vertical_scan_lines_opacity : hint_range(0.0, 1.0, 0.01) = 1.0;
uniform float boost : hint_range(1.0, 2.0, 0.01) = 1.2;
uniform float aberration_amount : hint_range(-10.0, 10.0, 0.01) = 0.0;

vec2 uv_curve(vec2 uv) {
	if (show_curvature) {
		uv = uv * 2.0 - 1.0;
		vec2 offset = abs(uv.yx) / vec2(curvature_x_amount, curvature_y_amount);
		uv = uv + uv * offset * offset;
		uv = uv * 0.5 + 0.5;
	}

	return uv;
}


void fragment() {
	vec2 uv = uv_curve(UV);
	vec2 screen_uv = uv_curve(SCREEN_UV);
	vec3 color = texture(SCREEN_TEXTURE, screen_uv).rgb;

	if (aberration_amount > 0.0) {
		float adjusted_amount = aberration_amount / screen_size.x;
		color.r = texture(SCREEN_TEXTURE, vec2(screen_uv.x + adjusted_amount, screen_uv.y)).r;
		color.g = texture(SCREEN_TEXTURE, screen_uv).g;
		color.b = texture(SCREEN_TEXTURE, vec2(screen_uv.x - adjusted_amount, screen_uv.y)).b;
	}

	if (show_vignette) {
		float vignette = uv.x * uv.y * (1.0 - uv.x) * (1.0 - uv.y);
		vignette = clamp(pow((screen_size.x / 4.0) * vignette, vignette_opacity), 0.0, 1.0);
		color *= vignette;
	}

	if (show_horizontal_scan_lines) {
		float s = sin(screen_uv.y * horizontal_scan_lines_amount * PI * 2.0);
		s = (s * 0.5 + 0.5) * 0.9 + 0.1;
		vec4 scan_line = vec4(vec3(pow(s, horizontal_scan_lines_opacity)), 1.0);
		color *= scan_line.rgb;
	}

	if (show_vertical_scan_lines) {
		float s = sin(screen_uv.x * vertical_scan_lines_amount * PI * 2.0);
		s = (s * 0.5 + 0.5) * 0.9 + 0.1;
		vec4 scan_line = vec4(vec3(pow(s, vertical_scan_lines_opacity)), 1.0);
		color *= scan_line.rgb;
	}

	if (show_horizontal_scan_lines || show_vertical_scan_lines) {
		color *= boost;
	}

	// Fill the blank space of the corners, left by the curvature, with black.
	if (uv.x < 0.0 || uv.x > 1.0 || uv.y < 0.0 || uv.y > 1.0) {
		color = corner_color.rgb;
	}

	COLOR = vec4(color, 1.0);
}"

[sub_resource type="ShaderMaterial" id=2]
shader = SubResource( 1 )
shader_param/screen_size = Vector2( 112, 160 )
shader_param/show_curvature = false
shader_param/curvature_x_amount = 6.0
shader_param/curvature_y_amount = 4.0
shader_param/corner_color = Color( 0.12549, 0.25098, 0.317647, 1 )
shader_param/show_vignette = false
shader_param/vignette_opacity = 0.45
shader_param/show_horizontal_scan_lines = false
shader_param/horizontal_scan_lines_amount = 160.0
shader_param/horizontal_scan_lines_opacity = 1.0
shader_param/show_vertical_scan_lines = false
shader_param/vertical_scan_lines_amount = 320.0
shader_param/vertical_scan_lines_opacity = 1.0
shader_param/boost = 1.2
shader_param/aberration_amount = 0.0

[sub_resource type="Animation" id=3]
resource_name = "glitch"
length = 4.0
loop = true
tracks/0/type = "bezier"
tracks/0/path = NodePath("Crt/Screen/ColorRect:material:shader_param/aberration_amount")
tracks/0/interp = 1
tracks/0/loop_wrap = true
tracks/0/imported = false
tracks/0/enabled = true
tracks/0/keys = {
"points": PoolRealArray( 0, -0.25, 0, 0.25, 0, 1, -0.25, 0, 0.25, 0, 0, -0.25, 0, 0.25, 0 ),
"times": PoolRealArray( 0, 2, 4 )
}

[node name="Background" type="Node2D"]
script = ExtResource( 2 )

[node name="ParallaxBackground" type="ParallaxBackground" parent="."]
layer = -49
script = ExtResource( 4 )
speed = 93.0

[node name="ParallaxLayer" type="ParallaxLayer" parent="ParallaxBackground"]
motion_mirroring = Vector2( 112, 160 )

[node name="Sprite" type="Sprite" parent="ParallaxBackground/ParallaxLayer"]
texture = ExtResource( 1 )

[node name="ParallaxBackground2" type="ParallaxBackground" parent="."]
layer = -50
script = ExtResource( 4 )
speed = 47.0

[node name="ParallaxLayer" type="ParallaxLayer" parent="ParallaxBackground2"]
motion_mirroring = Vector2( 112, 160 )

[node name="Sprite" type="Sprite" parent="ParallaxBackground2/ParallaxLayer"]
texture = ExtResource( 3 )

[node name="ParallaxBackground3" type="ParallaxBackground" parent="."]
layer = -51
script = ExtResource( 4 )
speed = 22.0

[node name="ParallaxLayer" type="ParallaxLayer" parent="ParallaxBackground3"]
motion_mirroring = Vector2( 112, 160 )

[node name="Sprite" type="Sprite" parent="ParallaxBackground3/ParallaxLayer"]
texture = ExtResource( 5 )

[node name="Tint" type="CanvasLayer" parent="."]
layer = -45

[node name="ColorRect" type="ColorRect" parent="Tint"]
anchor_right = 1.0
anchor_bottom = 1.0
color = Color( 0.12549, 0.25098, 0.317647, 0.784314 )
__meta__ = {
"_edit_use_anchors_": false
}

[node name="Crt" parent="." instance=ExtResource( 6 )]
layer = -40

[node name="ColorRect" parent="Crt/Screen" index="0"]
material = SubResource( 2 )

[node name="AnimationPlayer" type="AnimationPlayer" parent="."]
anims/glitch = SubResource( 3 )

[editable path="Crt"]
