extends Area2D

var bounds 

func _ready():
	var shape = $CollisionShape2D
	var top_left = global_position - shape.shape.extents
	var size = 2 * shape.shape.extents
	bounds = Rect2(top_left, size)

func _on_area_exited(area):
	var obj = area.get_parent()
	# Make sure that we are only destroying enemies and bullets
	if obj is Enemy:
		# queue free instead of destroy(), as we dont want to trigger singals for score etc.
		if _is_pos_outside_bounds(obj.global_position):
			obj.queue_free()
	elif obj is Bullet:
		if _is_pos_outside_bounds(obj.global_position):
			obj.destroy()
	else:
		print("Object %s is out of screen bounds, but WILL NOT be destroyed" % obj.name)

func _is_pos_outside_bounds(pos):
	if  pos.x > bounds.end.x or \
		pos.x < bounds.position.x or \
		pos.y > bounds.end.y or \
		pos.y < bounds.position.y:
		return true
	return false
