extends Node2D

signal finished_spawning()
signal enemy_destroyed()

export(PackedScene) var enemy
export(float, 0.1, 10.0) var spawn_rate = 1.0
export(int, 1, 10) var max_spawned = 2
export(int, 1, 100) var total_spawned = 10

onready var spawn_timer = $SpawnTimer

var enemies = []
var world

var total_destroyed = 0

func initialize(_world):
	world = _world
	spawn_timer.wait_time = spawn_rate

func start_spawning():
	destroy_all_entities()
	spawn_timer.start()

func stop_spawning():
	spawn_timer.stop()
	for e in enemies:
		e.disable_actions()
#	yield(get_tree().create_timer(0.5), "timeout")
#	for e in enemies:
#		e.destroy_active_bullets()
#		e.queue_free()

func destroy_all_entities():
	for e in enemies:
		e.destroy_active_bullets()
		e.queue_free()

func reset():
	total_destroyed = 0

func spawn_enemy():
	if not enemy:
		print("HEY! No enemy set to spawn")
		spawn_timer.stop()
		emit_signal("finished_spawning")
		return
	if len(enemies) >= max_spawned:
		return
	if len(enemies) >= total_spawned - total_destroyed:
		return
	var new_enemy = enemy.instance()
	enemies.append(new_enemy)
	new_enemy.connect("tree_exiting", self, "_on_enemy_left_screen", [new_enemy])
	new_enemy.connect("actor_destroyed", self, "_on_enemy_destroyed")
	new_enemy.connect("actor_destroyed", world, "_on_enemy_destroyed")
	new_enemy.initialize(world.get_random_enemy_spawn_position())
	add_child(new_enemy)

func _on_SpawnTimer_timeout():
	spawn_enemy()

func _on_enemy_left_screen(e):
	enemies.erase(e)

func _on_enemy_destroyed(e):
	enemies.erase(e)
	total_destroyed += 1
	emit_signal("enemy_destroyed")
	if total_destroyed >= total_spawned:
		spawn_timer.stop()
		emit_signal("finished_spawning")
