extends Node2D

signal wave_progress_updated(current, total)
signal last_wave_completed()
signal wave_started(idx)

onready var waves = [
#	$Wave1,
##	$Wave2,
##	$Wave3,
##	$WaveBoss,
]

var current_wave_idx = -1

var world

func initialize(_world):
	world = _world
	waves = get_children()
	for w in waves:
		w.initialize(world)


func start_next_wave():
	if current_wave_idx > -1:
		waves[current_wave_idx].disconnect("wave_finished", self, "_on_wave_finished")
		waves[current_wave_idx].disconnect("wave_progress_updated", self, "_on_wave_progress_updated")
	current_wave_idx += 1
	if current_wave_idx >= len(waves):
		emit_signal("last_wave_completed")
	else:
		waves[current_wave_idx].connect("wave_finished", self, "_on_wave_finished")
		waves[current_wave_idx].connect("wave_progress_updated", self, "_on_wave_progress_updated")
		waves[current_wave_idx].start_wave()
		emit_signal("wave_started", current_wave_idx)

func stop_current_wave():
	if current_wave_idx >= len(waves):
		print("HEY! trying to stop wave with too big index %s" %current_wave_idx)
		return
	waves[current_wave_idx].stop_wave()

func restart_current_wave():
	if current_wave_idx >= len(waves):
		print("HEY! trying to restart wave with too big index %s" %current_wave_idx)
		return
	waves[current_wave_idx].stop_wave()
	waves[current_wave_idx].start_wave()

func _on_wave_finished():
#	print("Wave %s finished. Starting next wave" %(current_wave_idx + 1))
	start_next_wave()

func _on_wave_progress_updated(current, total):
	emit_signal("wave_progress_updated", current, total)
