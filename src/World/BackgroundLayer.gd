extends Node2D

export(Texture) var sprite = null
export(float) var sprite_size_y = 0.0
export(float) var move_scale_y = 1.0

var back_sprite = null
var front_sprite = null

func _ready():
	back_sprite = Sprite.new()
	add_child(back_sprite)
	back_sprite.texture = sprite
	back_sprite.position = Vector2(0.0, -sprite_size_y)
	
	front_sprite = Sprite.new()
	add_child(front_sprite)
	front_sprite.texture = sprite
	front_sprite.position = Vector2(0.0, 0.0)

func set_offset(offset_y: float):
	var scaled_offset = offset_y * move_scale_y
	if scaled_offset_y <
	back_sprite.position = Vector2(0.0, offset - sprite_size_y)
	front_sprite.position = Vector2(0.0, offset)
