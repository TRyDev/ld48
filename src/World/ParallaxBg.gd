extends ParallaxBackground

export(float, 0.0, 100.0) var speed = 10.0
const MAX_OFFSET = 640

func _ready():
	offset.y = MAX_OFFSET

func _process(delta):
	offset.y -= delta * speed
	if offset.y < 0:
		offset.y += MAX_OFFSET
