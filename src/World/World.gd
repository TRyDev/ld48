extends Node2D

signal score_updated(score)
signal player_deaths_updated(count)

onready var camera = $CameraController
onready var player_bounds = $PlayerBounds
onready var spawn_bounds = $SpawnBounds
onready var ui = $UserInterface
onready var wave_controller = $WaveController

var bounds_rect
var spawn_rect

var score = 0
var player_deaths = 0

var player_prefab = preload("res://src/Actors/Player.tscn")
var player = null

const PLAYER_START_OFFSET = Vector2(0.0, -50.0)
const PLAYER_START_FORCE = Vector2(0.0, 400.0)
const PLAYER_START_IMPULSE_COUNT = 40

var end_scene = preload("res://src/EndScene.tscn")

const START_DELAY = 1.0
const LAST_WAVE_DELAY = 2.0

func _ready():
	connect("score_updated", ui, "_on_score_updated")
	connect("player_deaths_updated", ui, "_on_player_deaths_updated")
	wave_controller.connect("wave_progress_updated", ui, "_on_wave_progress_updated")
	wave_controller.connect("last_wave_completed", self, "_on_last_wave_completed")
	wave_controller.connect("wave_started", $Crt, "_on_wave_started")
	_init_bound_rect()
	_init_spawn_rect()

	wave_controller.initialize(self)
	
	Courtain.play("show", true)
	yield(get_tree().create_timer(START_DELAY), "timeout")
	# Initialize player, then start next wave
	yield(_spawn_player(), "completed")
	
	wave_controller.start_next_wave()

	score = 0
	player_deaths = 0
	emit_signal("score_updated", 0)
	emit_signal("player_deaths_updated", 0)

func _process(delta):
	_bound_player_position()

func _spawn_player():
	player = null
	var new_player = player_prefab.instance()
	add_child(new_player)
	new_player.global_position = player_bounds.global_position + PLAYER_START_OFFSET
	new_player.set_invincible(true)
	for i in range(PLAYER_START_IMPULSE_COUNT):
		new_player.apply_force(PLAYER_START_FORCE / PLAYER_START_IMPULSE_COUNT)
		yield(get_tree(), "idle_frame")
	new_player.connect("actor_destroyed", self, "_on_player_destroyed")
	yield(get_tree().create_timer(0.2), "timeout")
	new_player.set_invincible(false)
	player = new_player

func _init_bound_rect():
	var top_left = player_bounds.global_position - player_bounds.shape.extents
	var size = player_bounds.shape.extents * 2
	bounds_rect = Rect2(top_left, size)

func _init_spawn_rect():
	var top_left = spawn_bounds.global_position - spawn_bounds.shape.extents
	var size = spawn_bounds.shape.extents * 2
	spawn_bounds = Rect2(top_left, size)

func get_random_enemy_spawn_position() -> Vector2:
	return Rng.rand_in_rect2(spawn_bounds)

func _bound_player_position():
	if player == null:
		return
	if player.global_position.x > bounds_rect.end.x:
		player.global_position.x = bounds_rect.end.x
	elif player.global_position.x < bounds_rect.position.x:
		player.global_position.x = bounds_rect.position.x
		
	if player.global_position.y > bounds_rect.end.y:
		player.global_position.y = bounds_rect.end.y
	elif player.global_position.y < bounds_rect.position.y:
		player.global_position.y = bounds_rect.position.y

func _on_player_destroyed(p):
	player_deaths += 1
	emit_signal("player_deaths_updated", player_deaths)
	player = null
	_set_score(score - 10)
	wave_controller.stop_current_wave()
	
	for c in get_children():
		if c is Player:
			c.queue_free()
	Courtain.play("show")
	yield(get_tree().create_timer(2.0), "timeout")
	Courtain.play("show", true)
	_spawn_player()
	wave_controller.restart_current_wave()

func _on_enemy_destroyed(e):
	_set_score(score + 1)

func _set_score(new_score):
	score = new_score
	emit_signal("score_updated", score)

func _on_last_wave_completed():
	yield(get_tree().create_timer(LAST_WAVE_DELAY), "timeout")
	GameSettings.score = score
	Courtain.play("show")
	yield(Courtain.anim_player, "animation_finished")
	SceneController.load_scene(end_scene)
