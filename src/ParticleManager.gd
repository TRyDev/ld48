extends Node2D

var explosion_prefab = preload("res://src/Actors/Explosion.tscn")

func play_explosion(pos: Vector2):
	var expl = explosion_prefab.instance()
	expl.global_position = pos
	get_tree().root.add_child(expl)
