extends Control

onready var demos_container = $Demos

const DEMOS_DIR = "res://src/Demos"
const DEMO_SCENE_NAME = "Demo.tscn"

func _ready():
	_create_demo_buttons()

func _create_demo_buttons():
	var demos = FileSystem.get_directories(DEMOS_DIR)
	for demo in demos:
		var demo_scene = FileSystem.concat_path([DEMOS_DIR, demo, DEMO_SCENE_NAME])
		var btn = Button.new()
		btn.text = demo
		btn.connect("pressed", self, "_on_demo_button_pressed", [demo_scene])
		demos_container.add_child(btn)

func _on_demo_button_pressed(scene_path):
	SceneController.load_scene_by_path(scene_path)
