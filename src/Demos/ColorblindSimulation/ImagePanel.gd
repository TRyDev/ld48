extends PanelContainer

onready var label = $VBoxContainer/Label
onready var image = $VBoxContainer/PanelContainer/Image
onready var shader = $VBoxContainer/PanelContainer/Shader

enum DistortionType {
	default,
	protanope,
	deuteranope,
	tritanope,
	corrected
}

export(DistortionType) var distortion_type = DistortionType.default

const SHADER_PARAMS = [
	"apply_protanope",
	"apply_deuteranope",
	"apply_tritanope",
	"apply_correction"
]

func _ready():
	label.text = DistortionType.keys()[distortion_type]
	shader.material = shader.material.duplicate()
	set_shader()

func set_texture(tex: Texture) -> void:
	image.texture = tex

func set_shader():
	for param in SHADER_PARAMS:
		shader.material.set_shader_param(param, false)
	match distortion_type:
		DistortionType.protanope:
			shader.material.set_shader_param("apply_protanope", true)
		DistortionType.deuteranope:
			shader.material.set_shader_param("apply_deuteranope", true)
		DistortionType.tritanope:
			shader.material.set_shader_param("apply_tritanope", true)
		DistortionType.corrected:
			shader.material.set_shader_param("apply_correction", true)
