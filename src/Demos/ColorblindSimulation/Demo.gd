extends Control

onready var image_select = $HBoxContainer/UI/VBoxContainer/ImageSelect
onready var images_container = $HBoxContainer/GridContainer

const IMAGES_PATH = "res://src/Demos/ColorblindSimulation/Images"

# int (option index) -> texture
var images = {}

func _ready():
	load_images()
	if len(images) > 0:
		image_select.select(0)

func load_images():
	var files = FileSystem.get_files(IMAGES_PATH)
	for i in range(len(files)):
		var f = files[i]
		if f.ends_with(".png") or f.ends_with(".jpg"):
			var name = f.split(".")[0]
			image_select.add_item(name)
			images[i] = load(FileSystem.concat_path([IMAGES_PATH, f]))

func set_images(tex: Texture) -> void:
	for img_panel in images_container.get_children():
		img_panel.set_texture(tex)

func _on_ImageSelect_item_selected(index):
	set_images(images[index])
