extends Node2D

onready var pixelize = $Pixelize
onready var pixel_size_label = $GUI/Screen/PanelContainer/VBoxContainer/PixelSize/Size
onready var pixel_size_slider = $GUI/Screen/PanelContainer/VBoxContainer/PixelSize/Slider

func _ready():
	var start_px_size = pixelize.get_pixel_size()
	pixel_size_label.text = str(start_px_size)
	pixel_size_slider.value = start_px_size

func _process(delta):
	if Input.is_action_just_pressed("escape"):
		SceneController.load_scene_by_path("res://src/Demos/Main.tscn")

func _on_PixelSizeSlider_value_changed(value):
	pixel_size_label.text = str(value)
	pixelize.set_pixel_size(value)
