extends Button

func _ready():
	text = name

func _on_pressed() -> void:
	if has_method(name):
		call(name)
	else:
		push_error("HEY! UI button trying to call non-existing function: %s" % name)

func play_music() -> void:
	MusicController.play("prophecy")

func pause_music() -> void:
	MusicController.pause(true)

func unpause_music() -> void:
	MusicController.pause(false)

func stop_music() -> void:
	MusicController.stop()

func set_volume_default() -> void:
	MusicController.lerp_volume_default(0.0)

func set_volume_quiet() -> void:
	MusicController.lerp_volume(-40, 0.0)

func lerp_volume() -> void:
	var t = 2.0
	MusicController.lerp_volume(-40, t)
	yield(get_tree().create_timer(t), "timeout")
	MusicController.lerp_volume()

func play_sfx_0d() -> void:
	SfxController.play("box_key")

func play_sfx_2d_random_pitch() -> void:
	var point = _get_demo().get_random_screen_point()
	SfxController.play_at("click", point, true)

func play_courtain_flash() -> void:
	Courtain.play("flash")

func _get_demo():
	return get_tree().root.get_node("Demo")

func back_to_main():
	SceneController.load_scene_by_path("res://src/Demos/Main.tscn")
