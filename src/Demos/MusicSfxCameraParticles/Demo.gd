extends Node2D

onready var camera = $CameraController
onready var camera_target = $CameraTarget

func _ready() -> void:
	camera.set_target_instant(camera_target)
	camera.set_zoom_level("medium", false)

func _process(delta):
	if Input.is_action_just_pressed("space"):
		camera.add_trauma(0.3)
	if Input.is_action_just_pressed("mmb"):
		var pos = get_global_mouse_position()
		if Rng.randf() < 0.5:
			ParticleController.play("dust", pos)
		else:
			ParticleController.play("blood", pos)
	if Input.is_action_just_pressed("lmb"):
		camera.set_zoom_level("close")
	elif Input.is_action_just_pressed("rmb"):
		camera.set_zoom_level("far")
	elif Input.is_action_just_released("lmb") or Input.is_action_just_released("rmb"):
		camera.set_zoom_level("medium")

func _physics_process(delta):
	_update_camera_target_position()

func _update_camera_target_position() -> void:
	var mouse_pos = get_global_mouse_position()
	var mouse_dir = mouse_pos.normalized()
	var mouse_dist = mouse_pos.length()
	camera_target.set_offset(mouse_dir, mouse_dist)
	camera_target.update()

func get_random_screen_point() -> Vector2:
	var screen_size = get_viewport().size * camera.zoom
	var top_left = camera.global_position - 0.5 * screen_size
	var bottom_right = camera.global_position + 0.5 * screen_size
	return Rng.randv2(top_left, bottom_right)
