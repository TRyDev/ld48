extends Node2D

signal destroyed_flocker_count_updated(destroyed_count)
signal speedrun_started()
signal speedrun_ended()

onready var flockers = $Flockers
onready var camera = $CameraController
onready var flow_field = $FlowField
onready var gui = $GUI

var flocker_prefab = preload("res://src/Modules/Physics/Flocker.tscn")
var bullet_preafab = preload("res://src/Demos/FlockSpeedrun/Bullet.tscn")

const SPEEDRUN_FLOCKER_COUNT = 10
var destroyed_flocker_count = 0

func _ready() -> void:
	camera.set_zoom_level("medium", false)
	connect("destroyed_flocker_count_updated", gui, "_on_score_updated")
	connect("speedrun_started", gui, "_on_speedrun_started")
	connect("speedrun_ended", gui, "_on_speedrun_ended")
	flow_field.set_field_visible(true)

func _process(delta):
	if Input.is_action_just_pressed("escape"):
		SceneController.load_scene_by_path("res://src/Demos/Main.tscn")
	if Input.is_action_just_pressed("space"):
		_start_speedrun()
	elif Input.is_action_just_pressed("lmb"):
		_shoot(get_global_mouse_position())
	elif Input.is_action_just_pressed("rmb"):
		_spawn_flocker(get_global_mouse_position())
	for flocker in flockers.get_children():
		var force = flocker.flow(flow_field)
		flocker.apply_force(force)
		flocker.flock()
		flocker.update()

func _shoot(pos: Vector2):
	var new_bullet = bullet_preafab.instance()
	new_bullet.global_position = pos
	add_child(new_bullet)

func _start_speedrun():
	Speedrun.reset()
	Speedrun.start()
	for flocker in flockers.get_children():
		flocker.queue_free()
	yield(get_tree(), "idle_frame")
	update_destroyed_flocker_count(0)
	for i in range(SPEEDRUN_FLOCKER_COUNT):
		var pos = _get_random_screen_point()
		var new_flocker = _spawn_flocker(pos)
		new_flocker.connect("tree_exited", self, "_on_flocker_destroyed")
	flow_field.set_field_visible(false)
	emit_signal("speedrun_started")

func _spawn_flocker(pos: Vector2):
	var new_flocker = flocker_prefab.instance()
	new_flocker.global_position = pos
	flockers.add_child(new_flocker)
	return new_flocker

func _get_random_screen_point() -> Vector2:
	var screen_size = get_viewport().size * camera.zoom
	var top_left = camera.global_position - 0.5 * screen_size
	var bottom_right = camera.global_position + 0.5 * screen_size
	return Rng.randv2(top_left, bottom_right)

func _on_flocker_destroyed():
	update_destroyed_flocker_count(destroyed_flocker_count + 1)

func update_destroyed_flocker_count(val: int) -> void:
	destroyed_flocker_count = val
	if destroyed_flocker_count >= SPEEDRUN_FLOCKER_COUNT:
		flow_field.set_field_visible(true)
		emit_signal("speedrun_ended")
	emit_signal("destroyed_flocker_count_updated", destroyed_flocker_count)
