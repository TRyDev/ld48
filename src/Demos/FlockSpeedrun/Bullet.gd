extends Area2D

onready var anim_player = $AnimationPlayer


func _ready():
	AnimationController.reset(anim_player)
	yield(anim_player, "animation_finished")
	AnimationController.play(anim_player, "shoot", false)
	yield(anim_player, "animation_finished")
	queue_free()

func _on_body_entered(body):
	body.queue_free()
