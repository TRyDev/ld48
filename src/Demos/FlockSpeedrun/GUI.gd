extends CanvasLayer

onready var speedrun = $Screen/Speedrun
onready var score = speedrun.get_node("Score")
onready var time = speedrun.get_node("Time")


func _ready():
	speedrun.visible = false

func _process(delta):
	set_time(Speedrun.get_time_string())

func _on_speedrun_started():
	set_process(true)
	speedrun.visible = true

func _on_speedrun_ended():
	set_process(false)

func _on_score_updated(s):
	score.text = str(s)

func set_time(t: String) -> void:
	time.text = t
