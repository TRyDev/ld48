extends Node

onready var tracks = {
	"bass": $Bass,
	"metal_pipe": $MetalPipe,
	"sonar": $Sonar,
	"boss2": $Boss,
}

func _ready():
	for key in tracks.keys():
		var track = tracks[key]
		track.lerp_volume(-80, 0.0)
		track.play(key)

func set_track(name: String, value: bool) -> void:
	if not name in tracks:
		print("HEY! Music %s not found" %name)
		return
	var track = tracks[name]
	if value:
		track.lerp_volume_default()
	else:
		track.lerp_volume(-80)
	if name == "boss2":
		set_track("sonar", false)
		set_track("metal_pipe", false)

func reset():
	for t in tracks.keys():
		set_track(t, false)
