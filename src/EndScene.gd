extends CanvasLayer

onready var score_label = $Screen/VBoxContainer/Score
onready var anim_player = $AnimationPlayer

const START_SCENE_PATH = "res://src/StartScene.tscn"

const TYPE_SFX_DURATION = 5.0
const TYPE_SFX_FREQ = 0.1

const START_DELAY = 1.0

const SEVEN_TTD_URL = "https://trydev.itch.io/7-turns-to-drown"

func _ready():
	set_process(false)
	Courtain.play("show", true)
	score_label.text = "Score: %s" %GameSettings.score
	AnimationController.reset(anim_player)
	yield(anim_player, "animation_finished")
	yield(get_tree().create_timer(START_DELAY), "timeout")
	_play_type_sfx()
	AnimationController.play(anim_player, "show", false)
	yield(anim_player, "animation_finished")
	set_process(true)
#
func _process(delta):
	if Input.is_action_just_pressed("restart"):
		GameSettings.score = 0
		Courtain.play("show")
		yield(Courtain.anim_player, "animation_finished")
		SceneController.load_scene_by_path(START_SCENE_PATH)
		MusicOrchestrator.reset()
		set_process(false)
	if Input.is_action_just_pressed("space"):
		OS.shell_open(SEVEN_TTD_URL)

func _play_type_sfx():
	var t = 0.0
	while t < TYPE_SFX_DURATION:
		SfxController.play("type_text")
		yield(get_tree().create_timer(TYPE_SFX_FREQ), "timeout")
		t += TYPE_SFX_FREQ
