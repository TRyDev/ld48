extends CanvasLayer

onready var anim_player = $AnimationPlayer

const WORLD_SCENE_PATH = "res://src/World/World.tscn"

const TYPE_SFX_DURATION = 7.0
const TYPE_SFX_FREQ = 0.1

const START_DELAY = 2.0

func _ready():
	MusicController.lerp_volume(-60, 0.0)
	MusicController.play("bass")
	MusicController.lerp_volume_default()
	set_process(false)
	Courtain.play("show", true)
	AnimationController.reset(anim_player)
	yield(anim_player, "animation_finished")
	yield(get_tree().create_timer(START_DELAY), "timeout")
	_play_type_sfx()
	AnimationController.play(anim_player, "show", false)
	yield(anim_player, "animation_finished")
	set_process(true)

func _process(delta):
	if Input.is_action_just_pressed("space"):
		set_process(false)
		Courtain.play("show")
		SfxController.play("click")
		yield(Courtain.anim_player, "animation_finished")
		SceneController.load_scene_by_path(WORLD_SCENE_PATH)

func _play_type_sfx():
	var t = 0.0
	while t < TYPE_SFX_DURATION:
		SfxController.play("type_text")
		yield(get_tree().create_timer(TYPE_SFX_FREQ), "timeout")
		t += TYPE_SFX_FREQ
